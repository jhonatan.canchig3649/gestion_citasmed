<?php
  class Usuarios extends CI_Controller{
      public function __construct(){
          parent::__construct();
          $this->load->model("usuario");
          $this->load->model("perfil");
          //validacion
      }
      public function index(){
          $data["listadoUsuarios"]=$this->usuario->consultarTodos();
          $this->load->view('header');
          $this->load->view('usuarios/index',$data);
          $this->load->view('footer');
      }
      //funcion Nuevo
      public function nuevo(){
        //modelo
          $data["listadoPerfiles"]=$this->perfil->consultarTodos();
          $this->load->view('header');
          $this->load->view('usuarios/nuevo',$data);
          $this->load->view('footer');
      }
      //funcion editar
      public function editar($id_usu){
        //modelo
          $data["listadoPerfiles"]=$this->perfil->consultarTodos();
          $data["usuario"]=$this->usuario->consultarPorId($id_usu);
          $this->load->view('header');
          $this->load->view('usuarios/editar',$data);
          $this->load->view('footer');
      }
      public function procesarActualizacion(){
        $id_usu=$this->input->post("id_usu");
        $datosUsuarioEditado=array(
            "identificacion_usu"=>$this->input->post("identificacion_usu"),
            "apellido_usu"=>$this->input->post("apellido_usu"),
            "nombre_usu"=>$this->input->post("nombre_usu"),
            "direccion_usu"=>$this->input->post("direccion_usu"),
            "telefono_usu"=>$this->input->post("telefono_usu"),
            "email_usu"=>$this->input->post("email_usu"),
            "password_usu"=>$this->input->post("password_usu"),
            "estado_usu"=>$this->input->post("estado_usu"),
            "fk_id_per"=>$this->input->post("fk_id_per"),
          );
          if ($this->usuario->actualizar($id_usu,$datosUsuarioEditado)){
            $this->session->set_flashdata("confirmacion","Usuario Actualizado exitosamente.");
          } else {
              $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
          }
          redirect("usuarios/index");
      }
      //registro de usuarios
      public function guardarUsuario(){
        $datosNuevoUsuario=array(
          "identificacion_usu"=>$this->input->post("identificacion_usu"),
          "apellido_usu"=>$this->input->post("apellido_usu"),
          "nombre_usu"=>$this->input->post("nombre_usu"),
          "direccion_usu"=>$this->input->post("direccion_usu"),
          "telefono_usu"=>$this->input->post("telefono_usu"),
          "email_usu"=>$this->input->post("email_usu"),
          "password_usu"=>$this->input->post("password_usu"),
          "estado_usu"=>$this->input->post("estado_usu"),
          "fk_id_per"=>$this->input->post("fk_id_per"),
        );
        if ($this->usuario->insertar($datosNuevoUsuario)) {
          //nombre variable contenido variable
              $this->session->set_flashdata("confirmacion","Usuario insertado exitosamente.");
        } else {
              $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
        }
        redirect("usuarios/index");
      }

      function procesarEliminacion($id_usu){
              if ($this->usuario->eliminar($id_usu)) {
                $this->session->set_flashdata("confirmacion","Usuario eliminado exitosamente.");
              } else {
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("usuarios/index");
      }
    }//cierre funcion
 ?>
