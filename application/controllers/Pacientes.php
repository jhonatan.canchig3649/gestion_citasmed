<?php
  class Pacientes extends CI_Controller{
      public function __construct(){
          parent::__construct();
          $this->load->model("paciente");
          //$this->load->model("perfil");
          //validacion
      }
      public function index(){
          $data["listadoPacientes"]=$this->paciente->consultarTodos();
          $this->load->view('header');
          $this->load->view('pacientes/index',$data);
          $this->load->view('footer');
      }
      //funcion Nuevo
      public function nuevo(){
        //modelo
          //$data["listadoPerfiles"]=$this->perfil->consultarTodos();
          $this->load->view('header');
          $this->load->view('pacientes/nuevo');
          $this->load->view('footer');
      }
      //funcion editar
      public function editar($id_pac){
        //modelo
          //$data["listadoPerfiles"]=$this->perfil->consultarTodos();
          $data["paciente"]=$this->paciente->consultarPorId($id_pac);
          $this->load->view('header');
          $this->load->view('pacientes/editar',$data);
          $this->load->view('footer');
      }
      public function procesarActualizacion(){
        $id_pac=$this->input->post("id_pac");
        $datosPacienteEditado=array(
            "identificacion_pac"=>$this->input->post("identificacion_pac"),
            "apellido_pac"=>$this->input->post("apellido_pac"),
            "nombre_pac"=>$this->input->post("nombre_pac"),
            "sexo_pac"=>$this->input->post("sexo_pac"),
            "edad_pac"=>$this->input->post("edad_pac"),
            "estado_civil_pac"=>$this->input->post("estado_civil_pac"),
            "email_pac"=>$this->input->post("email_pac"),
            "telefono_pac"=>$this->input->post("telefono_pac"),
            "direccion_pac"=>$this->input->post("direccion_pac"),
            "estado_pac"=>$this->input->post("estado_pac"),
          );
          if ($this->paciente->actualizar($id_pac,$datosPacienteEditado)){
            $this->session->set_flashdata("confirmacion","Paciente Actualizado exitosamente.");
          } else {
              $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
          }
          redirect("pacientes/index");
      }
      //registro de pacientes
      public function guardarPaciente(){
        $datosNuevoPaciente=array(
            "identificacion_pac"=>$this->input->post("identificacion_pac"),
            "apellido_pac"=>$this->input->post("apellido_pac"),
            "nombre_pac"=>$this->input->post("nombre_pac"),
            "sexo_pac"=>$this->input->post("sexo_pac"),
            "edad_pac"=>$this->input->post("edad_pac"),
            "estado_civil_pac"=>$this->input->post("estado_civil_pac"),
            "email_pac"=>$this->input->post("email_pac"),
            "telefono_pac"=>$this->input->post("telefono_pac"),
            "direccion_pac"=>$this->input->post("direccion_pac"),
            "estado_pac"=>$this->input->post("estado_pac"),
          );
        if ($this->paciente->insertar($datosNuevoPaciente)) {
          //nombre variable contenido variable
              $this->session->set_flashdata("confirmacion","Paciente insertado exitosamente.");
        } else {
              $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
        }
        redirect("pacientes/index");
      }

      function procesarEliminacion($id_pac){
              if ($this->paciente->eliminar($id_pac)) {
                $this->session->set_flashdata("confirmacion","Paciente eliminado exitosamente.");
              } else {
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("pacientes/index");
      }
    }//cierre funcion
 ?>
