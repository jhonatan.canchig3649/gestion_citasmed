<?php
    //constructor
    class Perfil extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }

        public function insertar($datos){
          return $this->db->insert('perfil',$datos);
        }

        public function actualizar($id_per,$datos){
          $this->db->where("id_per",$id_per);
            return $this->db->update("perfil",$datos);
        }

        public function consultarPorId($id_per){
          $this->db->where("id_per",$id_per);
            $perfil=$this->db->get('perfil');
            if ($perfil->num_rows()>0) {
                // Cuando si hay clientes registrados
                return $perfil->row();
            } else {
                //cuando no hay Clientes
                return false;
            }
        }

        //funcion para consultar
        public function consultarTodos(){
          //variable
            $listadoPerfiles=$this->db->get('perfil');
            if ($listadoPerfiles->num_rows()>0) {
                // Cuando si hay  registrados
                return $listadoPerfiles;
            } else {
                //cuando no hay
                return false;
            }
        }

        public function eliminar($id_per){
          $this->db->where("id_per",$id_per);
          return $this->db->delete("perfil");
        }
    }//cierre llave



 ?>
