<?php
    //constructor
    class Paciente extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }
        //funcion para insetar datos
        public function insertar($datos){
            return $this->db->insert('paciente',$datos);

        }

        public function actualizar($id_pac,$datos){
          $this->db->where("id_pac",$id_pac);
            return $this->db->update("paciente",$datos);
        }

        public function consultarPorId($id_pac){
          $this->db->where("id_pac",$id_pac);
          //$this->db->join("perfil","perfil.id_per=usuario.fk_id_per");
            $paciente=$this->db->get('paciente');
            if ($paciente->num_rows()>0) {
                // Cuando si hay registrados
                return $paciente->row();
            } else {
                //cuando no hay registros
                return false;
            }

        }

        //funcion para consultar
        public function consultarTodos(){
          //$this->db->join("perfil","perfil.id_per=usuario.fk_id_per");
            $listadoPacientes=$this->db->get('paciente');
            if ($listadoPacientes->num_rows()>0) {
                // Cuando si hay registrados
                return $listadoPacientes;
            } else {
                //cuando no hay registros
                return false;
            }
        }

        public function eliminar($id_pac){
          $this->db->where("id_pac",$id_pac);
          return $this->db->delete("paciente");
        }
    }
 ?>
