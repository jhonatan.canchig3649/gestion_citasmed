<?php
    //constructor
    class Usuario extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }
        //funcion para insetar datos
        public function insertar($datos){
            return $this->db->insert('usuario',$datos);

        }

        public function actualizar($id_usu,$datos){
          $this->db->where("id_usu",$id_usu);
            return $this->db->update("usuario",$datos);
        }

        public function consultarPorId($id_usu){
          $this->db->where("id_usu",$id_usu);
          $this->db->join("perfil","perfil.id_per=usuario.fk_id_per");
            $usuario=$this->db->get('usuario');
            if ($usuario->num_rows()>0) {
                // Cuando si hay registrados
                return $usuario->row();
            } else {
                //cuando no hay registros
                return false;
            }

        }

        //funcion para consultar
        public function consultarTodos(){
          $this->db->join("perfil","perfil.id_per=usuario.fk_id_per");
            $listadoUsuarios=$this->db->get('usuario');
            if ($listadoUsuarios->num_rows()>0) {
                // Cuando si hay registrados
                return $listadoUsuarios;
            } else {
                //cuando no hay registros
                return false;
            }
        }

        public function eliminar($id_usu){
          $this->db->where("id_usu",$id_usu);
          return $this->db->delete("usuario");
        }
    }
 ?>
