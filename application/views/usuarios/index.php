<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="text-center">
          <h1 class="card-title">USUARIOS</h1>
          <a href="<?php echo site_url(); ?>/usuarios/nuevo" class="btn btn-primary" ><i class="fa fa-plus"></i> Agregar Nuevo</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="d-flex aling-items-center mb-4">
          <h4 class="card-title"> USUARIOS REGISTRADOS</h4>
        </div>
        <?php if ($listadoUsuarios): ?>
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="tbl-usuarios">
              <thead>
                <tr>
                  <th class="text-center card-title">ID</th>
                  <th class="text-center card-title">IDENTIFICACION</th>
                  <th class="text-center card-title">APELLIDOS</th>
                  <th class="text-center card-title">NOMBRES</th>
                  <th class="text-center card-title">DIRECCION</th>
                  <th class="text-center card-title">TELEFONO</th>
                  <th class="text-center card-title">CORREO ELECTRONICO</th>
                  <th class="text-center card-title">ESTADO</th>
                  <th class="text-center card-title">PERFIL</th>
                  <th class="text-center card-title">OPCIONES</th>
                </tr>
              </thead>

              <tbody>
                <?php foreach ($listadoUsuarios->result() as $filaTemporal): ?>
                  <tr>
                    <td class="text-center"><?php echo $filaTemporal->id_usu ?></td>
                    <td class="text-center"><?php echo $filaTemporal->identificacion_usu ?></td>
                    <td class="text-center"><?php echo $filaTemporal->apellido_usu ?></td>
                    <td class="text-center"><?php echo $filaTemporal->nombre_usu ?></td>
                    <td class="text-center"><?php echo $filaTemporal->direccion_usu ?></td>
                    <td class="text-center"><?php echo $filaTemporal->telefono_usu ?></td>
                    <td class="text-center"><?php echo $filaTemporal->email_usu ?></td>
                    <td class="text-center">
                      <?php if ($filaTemporal->estado_usu=="ACTIVO"): ?>
                        <div class="alert alert-success">ACTIVO</div>
                      <?php else: ?>
                        <div class="alert alert-danger">INACTIVO</div>
                      <?php endif; ?>
                    </td>
                    <td class="text-center">
                      <?php if ($filaTemporal->nombre_per=="ADMINISTRADOR"): ?>
                        <div class="alert alert-warning">ADMINISTRADOR</div>
                      <?php endif; ?>
                      <?php if ($filaTemporal->nombre_per=="SECRETARIO/A"): ?>
                        <div class="alert alert-info">SECRETARIO/A</div>
                      <?php endif; ?>
                      <?php if ($filaTemporal->nombre_per=="MEDICO"): ?>
                        <div class="alert alert-primary">MEDICO</div>
                      <?php endif; ?>
                    </td>
                    <td class="text-center">
                      <a href="<?php echo site_url() ?>/usuarios/editar/<?php echo $filaTemporal->id_usu ?>" class="btn btn-warning"><i class="fa fa-pen"></i></a><br><br>
                      <a href="javascript:void()" class="btn btn-danger" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_usu ?>')"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        <?php else: ?>
          <div class="alert alert-danger text-center">
            <h2>No se encontraron usuarios registrados</h2>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    function confirmarEliminacion(id_usu){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el usuario de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/usuarios/procesarEliminacion/"+id_usu;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
