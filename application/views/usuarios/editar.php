<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="text-center">
          <br>
          <h2 class="card-title"> EDITAR USUARIO </h2><hr>
        </div>
        </div>
        <form action="<?php echo site_url() ?>/usuarios/procesarActualizacion" method="post">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-6">
                <input type="hidden" name="id_usu" id="id_usu" value="<?php echo $usuario->id_usu; ?>">
                <label for="" class="card-title">Identificacion:</label><br>
                <input type="number" name="identificacion_usu" id="identificacion_usu" value="<?php echo $usuario->identificacion_usu; ?>" placeholder="Ingrese el Numero de Cedula" class="form-control"><br>
                <label for="" class="card-title">Apellidos:</label><br>
                <input type="text" name="apellido_usu" id="apellido_usu" value="<?php echo $usuario->apellido_usu; ?>" placeholder="Ingrese los Apellidos" class="form-control"><br>
                <label for="" class="card-title">Nombres:</label><br>
                <input type="text" name="nombre_usu" id="nombre_usu" value="<?php echo $usuario->nombre_usu; ?>" placeholder="Ingrese los Nombres" class="form-control"><br>
                <label for="" class="card-title">Direccion:</label><br>
                <input type="text" name="direccion_usu" id="direccion_usu" value="<?php echo $usuario->direccion_usu; ?>" placeholder="Ingrese la Direccion" class="form-control"><br>
                <label for="" class="card-title">Numero Telefónico:</label><br>
                <input type="number" name="telefono_usu" id="telefono_usu" value="<?php echo $usuario->telefono_usu; ?>" placeholder="Ingrese el Telefono" class="form-control"><br>
                </div>
                <div class="col-md-6">
                <label for="" class="card-title">Correo Electronico:</label><br>
                <input type="text" name="email_usu" id="email_usu" value="<?php echo $usuario->email_usu; ?>" placeholder="Ingrese el Correo Electronico" class="form-control"><br>
                <label for="" class="card-title">Contraseña:</label><br>
                <input type="password" name="password_usu" id="password_usu" value="<?php echo $usuario->password_usu; ?>" placeholder="Ingrese la contraseña" class="form-control"><br>
                <label for="" class="card-title">Vuelva a ingresar la contraseña:</label><br>
                <input type="password" name="password_validacion" id="password_validacion" value="" placeholder="Vuelva a ingresar la contraseña" class="form-control"><br>
                <label for="" class="card-title">Estado:</label><br>
                <select class="form-control" name="estado_usu" id="estado_usu">
                    <option value="">Seleccione...</option>
                    <option value="ACTIVO">ACTIVO</option>
                    <option value="INACTIVO">INACTIVO</option>
                </select><br>
                <label for="" class="card-title">Perfil:</label><br>
                <select class="form-control" name="fk_id_per" id="fk_id_per">
                  <option value="">--Seleccione un perfil--</option>
                  <?php if ($listadoPerfiles): ?>
                        <?php foreach ($listadoPerfiles->result() as $perfilTemporal): ?>
                              <option value="<?php echo $perfilTemporal->id_per ?>">
                                  <?php echo $perfilTemporal->nombre_per ?>
                              </option>
                        <?php endforeach; ?>
                  <?php endif; ?>
              </select><br>
              </div>
            </div>
              <div class="text-center">
                <button type="submit" class="btn btn-success" name="button"> <i class="fa-solid fa-floppy-disk"></i> Actualizar</button>
                &nbsp;&nbsp;&nbsp
                <a href="<?php echo site_url(); ?>/usuarios/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
                <br><br>
              </div>
            </div>
          </div>
        <br>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#estado_usu").val("<?php echo $usuario->estado_usu ?>");
  $("#fk_id_per").val("<?php echo $usuario->fk_id_per ?>");
</script>
