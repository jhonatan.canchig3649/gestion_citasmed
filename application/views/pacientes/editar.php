<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="text-center">
          <br>
          <h2 class="card-title"> ACTUALIZAR PACIENTE </h2><hr>
        </div>
        </div>
        <form action="<?php echo site_url() ?>/pacientes/procesarActualizacion" method="post">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-6">
                <input type="hidden" name="id_pac" id="id_pac" value="<?php echo $paciente->id_pac; ?>">
                <label for="" class="card-title">Identificacion:</label><br>
                <input type="number" name="identificacion_pac" id="identificacion_pac" value="<?php echo $paciente->identificacion_pac; ?>" placeholder="Ingrese el Numero de Cedula" class="form-control"><br>
                <label for="" class="card-title">Apellidos:</label><br>
                <input type="text" name="apellido_pac" id="apellido_pac" value="<?php echo $paciente->apellido_pac; ?>" placeholder="Ingrese los Apellidos" class="form-control"><br>
                <label for="" class="card-title">Nombres:</label><br>
                <input type="text" name="nombre_pac" id="nombre_pac" value="<?php echo $paciente->nombre_pac; ?>" placeholder="Ingrese los Nombres" class="form-control"><br>
                <label for="" class="card-title">Sexo:</label><br>
                <select class="form-control" name="sexo_pac" id="sexo_pac">
                    <option value="">--- Seleccione uno ---</option>
                    <option value="MASCULINO">MASCULINO</option>
                    <option value="FEMENINO">FEMENINO</option>
                </select><br>
                <label for="" class="card-title">Edad:</label><br>
                <input type="number" name="edad_pac" id="edad_pac" value="<?php echo $paciente->edad_pac; ?>" placeholder="Ingrese la edad" class="form-control"><br>
              </div>
              <div class="col-md-6">
                <label for="" class="card-title">Estado Civil:</label><br>
                <select class="form-control" name="estado_civil_pac" id="estado_civil_pac">
                    <option value="">--- Seleccione uno ---</option>
                    <option value="SOLTERO">SOLTERO</option>
                    <option value="UNION LIBRE">UNION LIBRE</option>
                    <option value="CASADO">CASADO</option>
                    <option value="VIUDO">VIUDO</option>
                </select><br>
                <label for="" class="card-title">Correo Electronico:</label><br>
                <input type="text" name="email_pac" id="email_pac" value="<?php echo $paciente->email_pac; ?>" placeholder="Ingrese el Correo Electronico" class="form-control"><br>
                <label for="" class="card-title">Numero Telefonico:</label><br>
                <input type="number" name="telefono_pac" id="telefono_pac" value="<?php echo $paciente->telefono_pac; ?>" placeholder="Ingrese el numero de telefono" class="form-control"><br>
                <label for="" class="card-title">Direccion:</label><br>
                <input type="text" name="direccion_pac" id="direccion_pac" value="<?php echo $paciente->direccion_pac; ?>" placeholder="Ingrese la direccion" class="form-control"><br>
                <label for="" class="card-title">Estado:</label><br>
                <select class="form-control" name="estado_pac" id="estado_pac">
                    <option value="">--- Seleccione un estado ---</option>
                    <option value="ACTIVO">ACTIVO</option>
                    <option value="INACTIVO">INACTIVO</option>
                </select><br>
              </div>
            </div>
              <div class="text-center">
                <button type="submit" class="btn btn-success" name="button"> <i class="fa-solid fa-floppy-disk"></i> Actualizar</button>
                &nbsp;&nbsp;&nbsp
                <a href="<?php echo site_url(); ?>/pacientes/index" class="btn btn-danger"> <i class="fa fa-times"></i> Cancelar</a>
                <br><br>
              </div>
            </div>
          </div>
        <br>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#sexo_pac").val("<?php echo $paciente->sexo_pac ?>");
  $("#estado_civil_pac").val("<?php echo $paciente->estado_civil_pac ?>");
  $("#estado_pac").val("<?php echo $paciente->estado_pac ?>");
</script>
