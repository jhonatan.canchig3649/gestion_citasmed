<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="text-center">
          <h1 class="card-title">PACIENTES</h1>
          <a href="<?php echo site_url(); ?>/pacientes/nuevo" class="btn btn-primary" ><i class="fa fa-plus"></i> Agregar Nuevo</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="d-flex aling-items-center mb-4">
          <h4 class="card-title"> PACIENTES REGISTRADOS</h4>
        </div>
        <?php if ($listadoPacientes): ?>
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="tbl-pacientes">
              <thead>
                <tr>
                  <th class="text-center card-title">ID</th>
                  <th class="text-center card-title">IDENTIFICACION</th>
                  <th class="text-center card-title">APELLIDOS</th>
                  <th class="text-center card-title">NOMBRES</th>
                  <th class="text-center card-title">SEXO</th>
                  <th class="text-center card-title">EDAD</th>
                  <th class="text-center card-title">ESTADO CIVIL</th>
                  <th class="text-center card-title">CORREO ELECTRONICO</th>
                  <th class="text-center card-title">TELEFONO</th>
                  <th class="text-center card-title">DIRECCION</th>
                  <th class="text-center card-title">ESTADO</th>
                  <th class="text-center card-title">OPCIONES</th>
                </tr>
              </thead>

              <tbody>
                <?php foreach ($listadoPacientes->result() as $filaTemporal): ?>
                  <tr>
                    <td class="text-center"><?php echo $filaTemporal->id_pac ?></td>
                    <td class="text-center"><?php echo $filaTemporal->identificacion_pac ?></td>
                    <td class="text-center"><?php echo $filaTemporal->apellido_pac ?></td>
                    <td class="text-center"><?php echo $filaTemporal->nombre_pac ?></td>
                    <td class="text-center"><?php echo $filaTemporal->sexo_pac ?></td>
                    <td class="text-center"><?php echo $filaTemporal->edad_pac ?></td>
                    <td class="text-center"><?php echo $filaTemporal->estado_civil_pac ?></td>
                    <td class="text-center"><?php echo $filaTemporal->email_pac ?></td>
                    <td class="text-center"><?php echo $filaTemporal->telefono_pac ?></td>
                    <td class="text-center"><?php echo $filaTemporal->direccion_pac ?></td>
                    <td class="text-center">
                      <?php if ($filaTemporal->estado_pac=="ACTIVO"): ?>
                        <div class="alert alert-success">ACTIVO</div>
                      <?php else: ?>
                        <div class="alert alert-danger">INACTIVO</div>
                      <?php endif; ?>
                    </td>
                    <td class="text-center">
                      <a href="<?php echo site_url() ?>/pacientes/editar/<?php echo $filaTemporal->id_pac ?>" class="btn btn-warning"><i class="fa fa-pen"></i></a><br><br>
                      <a href="javascript:void()" class="btn btn-danger" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_pac ?>')"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        <?php else: ?>
          <div class="alert alert-danger text-center">
            <h2>No se encontraron pacientes registrados</h2>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    function confirmarEliminacion(id_pac){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el paciente de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/pacientes/procesarEliminacion/"+id_pac;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

<script type="text/javascript">
  $(document).ready( function () {
  	$('#tbl-pacientes').DataTable({
      dom: 'Blfrtip',
      buttons: [
          'copyHtml5',
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
      ],
    });
  } );
</script>
